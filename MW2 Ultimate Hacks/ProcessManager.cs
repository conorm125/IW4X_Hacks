﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Text;


namespace MW2_Ultimate_Hacks
{
    [Flags]
    public enum ProcessAccessFlags : uint
    {
        All = 0x001F0FFF,
        Terminate = 0x00000001,
        CreateThread = 0x00000002,
        VirtualMemoryOperation = 0x00000008,
        VirtualMemoryRead = 0x00000010,
        VirtualMemoryWrite = 0x00000020,
        DuplicateHandle = 0x00000040,
        CreateProcess = 0x000000080,
        SetQuota = 0x00000100,
        SetInformation = 0x00000200,
        QueryInformation = 0x00000400,
        QueryLimitedInformation = 0x00001000,
        Synchronize = 0x00100000
    }

    class ProcessManager
    {
        #region External Functions

        [DllImport("kernel32.dll", SetLastError = true)]
        private static extern IntPtr OpenProcess(
         ProcessAccessFlags processAccess,
         bool bInheritHandle,
         int processId
        );

        private static IntPtr OpenProcess(Process proc, ProcessAccessFlags flags)
        {
            return OpenProcess(flags, false, proc.Id);
        }

        [DllImport("kernel32.dll", SetLastError = true)]
        static extern bool ReadProcessMemory(
        IntPtr hProcess,
        IntPtr lpBaseAddress,
        [Out] byte[] lpBuffer,
        int dwSize,
        out IntPtr lpNumberOfBytesRead);

        [DllImport("kernel32.dll", SetLastError = true)]
        static extern bool ReadProcessMemory(
            IntPtr hProcess,
            IntPtr lpBaseAddress,
            [Out, MarshalAs(UnmanagedType.AsAny)] object lpBuffer,
            int dwSize,
            out IntPtr lpNumberOfBytesRead);

        [DllImport("kernel32.dll", SetLastError = true)]
        static extern bool ReadProcessMemory(
            IntPtr hProcess,
            IntPtr lpBaseAddress,
            IntPtr lpBuffer,
            int dwSize,
            out IntPtr lpNumberOfBytesRead);

        #endregion


        private IntPtr processHandle;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="processName">The name of the process to hook.</param>
        public ProcessManager(string processName)
        {
            Process process = Process.GetProcessesByName(processName)[0];
            processHandle = OpenProcess(ProcessAccessFlags.All, false, process.Id);
        }

        public float ReadFloat(IntPtr address)
        {
            // if the process handle is not open it will be null
            if(processHandle != null)
            {
                IntPtr bytesRead = (IntPtr)0;
                byte[] buffer = new byte[4];

                ReadProcessMemory(processHandle, address, buffer, buffer.Length, out bytesRead);

                return System.BitConverter.ToSingle(buffer, 0);
            }
            else
            {
                // Probally do some error stuff here
                return 0.0F;
            }
        }

    }
}
