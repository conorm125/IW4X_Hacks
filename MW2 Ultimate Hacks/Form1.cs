﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MW2_Ultimate_Hacks
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Attempts to hook the game's process.
        /// </summary>
        /// <returns>True if sucessful, False otherwise.</returns>
        public bool ConnectToGame()
        {
            ProcessManager pm = new ProcessManager("iw4x");
            MessageBox.Show(pm.ReadFloat((IntPtr)0x007F4310).ToString());
            return false;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ConnectToGame();
        }
    }
}
